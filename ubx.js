Bootstrapper.UBX = {};
var authKey = {{auth_key}};
var apiUrl = {{api_url}};

Bootstrapper.UBX.authenticationKey = authKey;
Bootstrapper.UBX.apiURL = apiUrl;

Bootstrapper.UBX.handleResponse = function(error, response, body, onSuccess, onFail) {
    if (error) {
        onFail(error);
        return;
    }
    try {
        var body = JSON.parse(body);
    } catch (e) {
        onFail("failed to parse body: \n" + body);
    }
    if (response.statusCode == 200) {
        onSuccess(body);
    } else {
        onFail(body.description);
    }
};
Bootstrapper.UBX.post = function(endpoint, options, onSuccess, onFail) {
    var args = {"endpoint": endpoint || "", "options": options || {}, "onSuccess": onSuccess || function() {}, "onFail": onFail || function() {}
        },
        xhr = new XMLHttpRequest();
    requestHeaders = {};
    requestHeaders["Authorization"] = "Bearer " + Bootstrapper.UBX.authenticationKey;
    requestHeaders["Content-Type"] = "application/json";
    xhr.open("POST", Bootstrapper.UBX.apiURL + args.endpoint);
    for (var key in requestHeaders) {
        if (!requestHeaders.hasOwnProperty(key)) continue;
        xhr.setRequestHeader(key, requestHeaders[key]);
    }

    xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
            Bootstrapper.UBX.handleResponse(null, {
                statusCode: this.status
            }, this.responseText, args.onSuccess, args.onFail);
        }
    };
    xhr.send(args.options.body);
};